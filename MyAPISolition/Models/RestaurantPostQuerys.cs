﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using MySqlConnector;

namespace MyAPISolition.Models
{
    public class RestaurantPostQuerys
    {
        public AppDB Db { get; set; }

        public RestaurantPostQuerys(AppDB db)
        {
            Db = db;
        }

        public async Task<Restaurant> FindByIdAsync(int Id)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM restaurants WHERE id = @id;";
            cmd.Parameters.Add(new MySqlParameter {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = Id
            });

            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<Restaurant>> FindLastAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM restaurants ORDER BY 'id' DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        private async Task<List<Restaurant>> ReadAllAsync(MySqlDataReader reader)
        {
            var restaurants = new List<Restaurant>();

            using (reader)
            {
                while(await reader.ReadAsync())
                {
                    var restaurant = new Restaurant(Db)
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        City = reader.GetString(2),
                        Address = reader.GetString(3),
                        Latitude = reader.GetString(4),
                        Longitude = reader.GetString(5),
                        Preferences = reader.GetString(6)
                    };
                    restaurants.Add(restaurant);
                }
            }
            return restaurants;
        }
    }
}
