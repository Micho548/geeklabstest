﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using MySqlConnector;

namespace MyAPISolition.Models
{
    public class UsersPostQuery
    {
        public AppDB Db { get; set; }

        public UsersPostQuery(AppDB db) {  Db = db; }

        public async Task<User> FindByIdAsync(int Id)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM users WHERE id = @id;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = Id
            });

            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<User>> FindLastAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM users ORDER BY 'id' DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        private async Task<List<User>> ReadAllAsync(MySqlDataReader reader)
        {
            var users = new List<User>();

            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var user = new User(Db)
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        City = reader.GetString(2),
                        ImgProfile = reader.GetString(3),
                        Address = reader.GetString(4),
                        Preferences = reader.GetString(5)
                    };
                    users.Add(user);
                }
            }
            return users;
        }
    }
}
