﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MyAPISolition.Models
{
    public class PersonaContext: DbContext
    {
        public PersonaContext(DbContextOptions<PersonaContext> options)
            :base(options)
        {
        }

        public DbSet<Persona> personas { get; set; }
    }
}
