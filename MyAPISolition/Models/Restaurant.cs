﻿using System.Data;
using System.Threading.Tasks;
using MySqlConnector;

namespace MyAPISolition.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Preferences { get; set; }

        internal AppDB Db { get; set; }

        public Restaurant()
        {
        }

        internal Restaurant(AppDB db)
        {
            this.Db = db;
        }

        public async Task InsertAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO 
                    'restaurants'('name','city','address','latitude','longitude','preferences')
                     VALUES(@name, @city, @address, @latitude, @longitude, @preferences);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE 'restaurants' 
                    SET 'name' = @name, 
                    'city' = @city,
                    'address' = @address,
                    'longitude' = @longitude,
                    'latitude' = @latitude,
                    'preferences' = @preferences;";
            BindParams(cmd);
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task DeleteAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM 'restaurants' WHERE 'id' = @id;";
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = Id
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter {
                ParameterName = "@name",
                DbType = DbType.String,
                Value = Name
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@city",
                DbType = DbType.String,
                Value = City
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@address",
                DbType = DbType.String,
                Value = Address
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@latitude",
                DbType = DbType.Int64,
                Value = Latitude
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@longitude",
                DbType = DbType.Int64,
                Value = Longitude
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@preferences",
                DbType = DbType.String,
                Value = Preferences
            });
        }
    }
}
