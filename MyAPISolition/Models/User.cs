﻿using System.Data;
using System.Threading.Tasks;
using MySqlConnector;

namespace MyAPISolition.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string ImgProfile { get; set; }
        public string Address { get; set; }
        public string Preferences { get; set; }

        internal AppDB Db { get; set; }

        public User(){}

        internal User(AppDB db) { this.Db = db; }

        public async Task InsertAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO 
              'users'('name', 'city', 'profile_img_url', 'address', 'profile_img_url')
              VALUES(@name, @city, @imgProfile, @address, @preferences)";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE 'users' 
                    SET 'name' = @name, 
                    'city' = @city,
                    'profile_img_url' = @imgProfile,
                    'address' = @address,
                    'preferences' = @preferences;";
            BindParams(cmd);
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task DeleteAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM users WHERE 'id' = @id;";
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = Id
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@name",
                DbType = DbType.String,
                Value = Name
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@city",
                DbType = DbType.String,
                Value = City
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@imgProfile",
                DbType = DbType.String,
                Value = ImgProfile
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@address",
                DbType = DbType.String,
                Value = Address
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@preferences",
                DbType = DbType.String,
                Value = Preferences
            });
        }
    }
}
