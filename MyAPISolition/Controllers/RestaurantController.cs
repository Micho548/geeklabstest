﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyAPISolition.Controllers
{
    [Route("api/[controller]")]
    public class RestaurantController: ControllerBase
    {
        public AppDB Db { get; set; }

        public RestaurantController(AppDB db)
        {
            Db = db;
        }

        // GET api/restaurant
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Db.Connection.OpenAsync();
            var query = new Models.RestaurantPostQuerys(Db);
            var result = await query.FindLastAsync();
            return new OkObjectResult(result);
        }
    }
}
