﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyAPISolition.Controllers
{
    [Route("api/[controller]")]
    public class UserController: ControllerBase
    {
        public AppDB Db { get; set; }

        public UserController(AppDB db){ Db = db; }

        // GET api/user
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Db.Connection.OpenAsync();
            var query = new Models.UsersPostQuery(Db);
            var result = await query.FindLastAsync();
            return new OkObjectResult(result);
        }
    }
}
